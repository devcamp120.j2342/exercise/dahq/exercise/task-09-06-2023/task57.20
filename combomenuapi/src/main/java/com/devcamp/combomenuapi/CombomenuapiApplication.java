package com.devcamp.combomenuapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CombomenuapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CombomenuapiApplication.class, args);
	}

}
